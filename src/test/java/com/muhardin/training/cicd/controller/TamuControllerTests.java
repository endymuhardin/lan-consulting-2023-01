package com.muhardin.training.cicd.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.jdbc.Sql;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql(
    scripts = {
        "classpath:/sql/clear-data-tamu.sql",
        "classpath:/sql/sample-data-tamu.sql"
    }
)
public class TamuControllerTests {
    @Value(value="${local.server.port}")
	private int port;

    @Test
    public void testFindAllTamu(){
        given().port(port)
        .get("/api/tamu/")
        .then()
        .body("", hasSize(5));
    }

    @Test
    public void testFindDataTamu(){
        given().port(port)
        .get("/api/tamu/")
        .then()
        .body("", hasItems(hasEntry("nama", "USer 902")));
    }
}
