package com.muhardin.training.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingCicdApplication.class, args);
	}

}
