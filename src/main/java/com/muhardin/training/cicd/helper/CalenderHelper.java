package com.muhardin.training.cicd.helper;

public class CalenderHelper {
    public static Integer hitungHari(Integer bulan, Integer tahun){
        if(bulan == 2) {
            if(tahun % 4 == 0) {
                return 29;
            }
            return 28;
        }
        return 30;
    }
}
