package com.muhardin.training.cicd.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.training.cicd.dao.TamuDao;
import com.muhardin.training.cicd.entity.Tamu;

@RestController 
public class TamuController {

    @Autowired
    private TamuDao tamuDao;

    @GetMapping("/api/tamu/")
    public Iterable<Tamu> dataTamu() {
        Iterable<Tamu> data = tamuDao.findAll(Sort.unsorted());
        return data;
    }
}
